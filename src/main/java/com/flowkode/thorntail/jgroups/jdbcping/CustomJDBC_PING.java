package com.flowkode.thorntail.jgroups.jdbcping;

import org.jgroups.annotations.Property;
import org.jgroups.conf.ClassConfigurator;
import org.jgroups.protocols.JDBC_PING;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;

public class CustomJDBC_PING extends JDBC_PING {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomJDBC_PING.class);

    @Property(description = "Milliseconds to sleep bewtween tries")
    protected int jndi_sleep = 500;

    @Property(description = "Number of times to try to acquire DataSource from JNDI")
    protected int jndi_tries = 10;

    private short customId = ClassConfigurator.getProtocolId(JDBC_PING.class);

    @Override
    public void init() throws Exception {
        this.id = customId;
        super.init();
    }

    @Override
    protected DataSource getDataSourceFromJNDI(String name) {
        int tries = jndi_tries;
        while (tries-- > 0) {
            try {
                return super.getDataSourceFromJNDI(name);
            }
            catch (IllegalArgumentException ex) {
                LOGGER.info("Failed to get datasource, retrying in {} ms", jndi_sleep);
                try {
                    Thread.sleep(jndi_sleep);
                }
                catch (InterruptedException e) {
                    LOGGER.error("Error Sleeping", e);
                }
            }
        }
        return super.getDataSourceFromJNDI(name);
    }

    @Override
    public short getId() {
        return customId;
    }
}
