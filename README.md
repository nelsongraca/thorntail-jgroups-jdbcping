This project is a small Thorntail fraction that provides an extension of the JDBC_PING protocol.

The need came from using Thorntail 2.5.0.Final where the datasource to be used by the JDBC_PING protocol
was initialized in parallel with the JGroups Stack, so in some situations it would not be present.
This custom implementation overrides the `getDataSourceFromJNDI` method of JDBC_PING and adds a retry mechanism.
  

In order to use it you will have to define it on your project-defaults.yml (or other) like this:

    thorntail:
      jgroups:
        stacks:
          tcp:
            protocols:
              com.flowkode.thorntail.jgroups.jdbcping.CustomJDBC_PING:
                module: com.flowkode.thorntail.jgroups.jdbcping
                properties:
                  jndi_tries: 10
                  jndi_sleep: 500

The properties jndi_tries and jndi_sleep are optional the defaults being 10 tries, sleeping 500ms between each try.

Further additions are welcome, either open an issue or submit a PR. 
